from flask import current_app, request
from flask_restful import Resource, abort
from db import User

import jwt
import functools
import datetime


def login_required(method):
    @functools.wraps(method)
    def wrapper(self, *args, **kwargs):
        token = request.headers.get("Authorisation")
        if token is None:
            abort(400, message="No authorisation header.")

        decoded = None

        try:
            decoded = jwt.decode(token.encode("utf-8"), current_app.config.get("SECRET"), algorithms="HS256")
        except jwt.DecodeError:
            abort(401, message="Token is not valid.")
        except jwt.ExpiredSignatureError:
            abort(401, message="Token is expired.")

        user = decoded.get("username")
        try:
            User.get(User.username == user)
        except User.DoesNotExist:
            abort(404, message="User not found!")

        try:
            return method(self, *args, user_requested=user, **kwargs)
        except TypeError:
            abort(400, message="Missing argument in url.")

    return wrapper


def get_user(username):
    user = None

    try:
        user = User.get(User.username == username)
    except User.DoesNotExist:
        abort(404, message="User not found.")

    return user


class Login(Resource):
    def post(self):
        username = request.form.get("username")
        password = request.form.get("password")

        if not all([username, password]):
            abort(400, message="Missing username/password.")

        try:
            user = User.get(User.username == username)
        except User.DoesNotExist:
            abort(404, message="User not found!")

        if user.verify_password(password):
            token = jwt.encode({
                "username": username,
                "exp": datetime.datetime.utcnow() + datetime.timedelta(hours=3)
            }, current_app.config.get("SECRET"), algorithm="HS256").decode("utf-8")
            return {"token": token}
        else:
            abort(403, message="Invalid credentials.")

    @login_required
    def get(self, user):
        return {"message": "You're logged in."}


class Register(Resource):
    def post(self):
        username = request.form.get("username")
        password = request.form.get("password")
        school = request.form.get("school")

        if not all([username, password, school]):
            abort(400, message="Missing a required parameter.")

        username_taken = True

        try:
            User.get(User.username == username)
        except User.DoesNotExist:
            username_taken = False

        if not username_taken:
            user = User()

            user.username = username
            user.update_password(password)

            user.type = 5
            user.school = school

            user.save()
        else:
            abort(400, message="Username is already taken.")

        return {"message": "Successfully registered."}

from flask import Flask, request
from flask_restful import Resource, Api

from login import Login, Register
from grades import GradesAPI
from db import initialise_database

app = Flask(__name__)
api = Api(app)

# region Setting up app variables

app.config.update({
    "SECRET": "mysecret",
})

# endregion End setting up variables

# region Setting up url handling

api.add_resource(Login, "/login")
api.add_resource(Register, "/register")

api.add_resource(
    GradesAPI,
    "/grades/<username>",               # GET, POST
    "/grades/<int:grade_id>",           # PUT, DELETE
)

# endregion

@app.before_first_request
def setup():
    initialise_database()


if __name__ == '__main__':
    app.run()

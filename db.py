from passlib.hash import pbkdf2_sha256

import peewee

db = peewee.SqliteDatabase("qportal.db")


class _BaseModel(peewee.Model):
    class Meta:
        database = db


class User(_BaseModel):
    username = peewee.CharField()
    password = peewee.CharField()
    type = peewee.IntegerField()
    school = peewee.CharField()

    def update_password(self, password):
        self.password = pbkdf2_sha256.hash(password)

    def verify_password(self, password):
        return pbkdf2_sha256.verify(password, self.password)


class Grade(_BaseModel):
    grade_id = peewee.AutoField()
    subject = peewee.CharField()
    grade = peewee.IntegerField()
    user = peewee.ForeignKeyField(User, backref="grades")


tables = [User, Grade]


def initialise_database():
    db.connect()
    db_fine = True
    for table in tables:
        if not db.table_exists(table):
            db_fine = False

    if not db_fine:
        db.create_tables(tables)

    print("Initialised database.")

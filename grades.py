from flask import current_app, request
from flask_restful import Resource, abort

from login import login_required, get_user
from db import User, Grade


class GradesAPI(Resource):
    @login_required
    def get(self, username, user_requested=None):
        user = get_user(username)
        req_user = get_user(user_requested)

        if req_user.type < 2 and req_user.username != username:
            abort(403, message="You are unauthorised to execute this request.")

        grades = {}
        for grade in user.grades:
            if grade.subject in grades.keys():
                grades[grade.subject].append([grade.grade_id, grade.grade])
            else:
                grades[grade.subject] = [[grade.grade_id, grade.grade]]

        return {
            "user": username,
            "grades": grades
        }

    @login_required
    def post(self, username, user_requested=None):
        user = get_user(username)
        req_user = get_user(user_requested)

        if req_user.type < 2:
            abort(403, message="You are unauthorised to execute this request.")

        subject = request.form.get("subject")
        grade = request.form.get("grade")

        if not subject or not grade:
            abort(400, message="Missing required arguments.")

        grade_o = Grade()
        grade_o.subject = subject
        grade_o.grade = grade
        grade_o.user = user
        grade_o.save()

        return {"message": "Added grade successfully."}

    @login_required
    def put(self, grade_id, user_requested=None):
        req_user = get_user(user_requested)

        if req_user.type < 2:
            abort(403, message="You are unauthorised to execute this request.")

        try:
            grade = Grade.get_by_id(grade_id)
        except Grade.DoesNotExist:
            abort(404, message="Grade not found.")

        new_grade = request.form.get("new_grade")

        grade.grade = new_grade
        grade.save()

        return {"message": "Updated grade successfully."}

    @login_required
    def delete(self, grade_id, user_requested=None):
        req_user = get_user(user_requested)

        if req_user.type < 2:
            abort(403, message="You are unauthorised to execute this request.")

        try:
            Grade.get_by_id(grade_id)
        except Grade.DoesNotExist:
            abort(404, message="Grade not found.")

        Grade.delete_by_id(grade_id)

        return {"message": "Removed grade successfully."}
